#!/bin/sh

if [ -f "/robles-io/pypicloud.ini" ]; then
    cat /robles-io/pypicloud.ini > /etc/pypicloud/config.ini
fi

uwsgi --die-on-term /etc/pypicloud/config.ini

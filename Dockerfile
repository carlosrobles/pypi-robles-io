FROM stevearc/pypicloud:1.3.3-alpine

LABEL maintainer="Carlos Robles carlos.robles@gmail.com"

RUN pip install --no-cache-dir --upgrade pypicloud[gcs]

USER root

RUN chown -R pypicloud:pypicloud /etc/pypicloud

USER pypicloud

ADD startup.sh /bin/

CMD ["/bin/startup.sh"]

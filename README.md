# pypi-robles-io

PyPiCloud server hosted on Cloud Run and available at https://pypi.robles.io

For more info on PyPiCloud:
- https://github.com/stevearc/pypicloud
- https://pypicloud.readthedocs.io
<br>

## What you'll Need
`docker`, `docker-compose`, and `make`.
<br>

## Usage
Launch a local instance of PyPiCloud using local filesystem storage:

`make up`
<br>

Visit the PypiCloud UI at [http://localhost:8080](http://localhost:8080). Register an administrator account.
<br>

You can now upload python packages to this local PyPiCloud with the newly created admin account:

```
# build source distribution and wheel for your python project
python setup.py sdist bdist_wheel

# write pypi config file
cat << EOF > ~/.pypirc
[distutils]
index-servers = pypicloud

[pypicloud]
repository: http://localhost:8080/simple
username: { YOUR PYPI ADMIN USERNAME }
password: { YOUR PYPI ADMIN PASSWORD }
EOF

# upload python sdist and wheel to local pypi
twine upload -r pypicloud --skip-existing dist/* --verbose
```

Stop and destroy the container:

`make down`
